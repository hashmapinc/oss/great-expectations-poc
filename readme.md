# Great Expectations POC

Table of Contents

* [What is Great Expectations?](#what-is-great-expectations)
* [Why would I use Great Expectations?](#why-would-i-use-great-expectations)
* [Key features](#Key-features)
* [Great Expectations use case](#great-expectation-use-case)
    * [Install the required packages](#install-the-required-packages)
    * [Initialize great expectations project](#initialize-great-expectation-project)
    * [Add data sources](#add-the-data-sources)
        * [MySQL](#mysql)
        * [FileSystem](#filesystem)
        * [AWS s3](#aws-s3)
        * [Snowflake](#snowflake)
    * [Great Expectations suite](#great-expectation-suite)
    * [Data Validation](#data-validation)
        * [Create a new checkpoint](#create-a-new-checkpoint)
        * [Run a checkpoint in the cli](#run-a-checkpoint-in-the-cli)
    * [Great Expectations in Prefect Workflow](#great-expectation-in-prefect-workflow)
        * [Add Prefect task to run checkpoints](#add-Prefect-task-to-run-checkpoints)
        * [Run Prefect Workflow](#run-prefect-workflow)
    * [Miscellaneous](#miscellaneous)


## What is Great Expectations?
Great Expectations is a useful tool to profile, validate, and document data. It helps to maintain the quality of data throughout a data workflow and pipeline.

Used with a workflow orchestration service, great expectations can help accelerate a data solution project by catching data issues at the earliest and notify data engineers to fix the issues.
## Why would I use Great Expectations?
For this section I felt, it's best to pick the lines from [docs.greatexpectations.io](https://docs.greatexpectations.io/en/latest/intro.html#:~:text=Great%20Expectations%20is%20Python-based.%20You%20can%20invoke%20it,environment,%20you%20might%20consider%20assertR%20as%20an%20alternative.)

To get more done with data, faster. Teams use Great Expectations to

* Save time during data cleaning and munging.
* Accelerate ETL and data normalization.

* Streamline analyst-to-engineer handoffs.

* Streamline knowledge capture and requirements gathering from subject-matter experts.

* Monitor data quality in production data pipelines and data products.

* Automate verification of new data deliveries from vendors and other teams.

* Simplify debugging data pipelines if (when) they break.

* Codify assumptions used to build models when sharing with other teams or analysts.

* Develop rich, shared data documentation in the course of normal work.

* Make implicit knowledge explicit.

… and much more

## Key features
* Expectations : They are like assertions in traditional python unit tests
* Automated data profiling : automated pipeline tests
* Data Contexts and Data Sources: allow you to configure connections to your data sources.
* Tooling for validation: Checkpoints for data validation
* Data Docs: clean, human-readable documentation

## Great Expectations use case

This is a poc on use of Great Expectations to validate data in a data pipeline. To show the workflow we have used Python Framework Prefect.
The various components are:
* MYSQL DB
* Local FS
* AWS S3 storage
* Snowflake

The ultimate aim is to load data into Snowflake.
Extract data from MYSQLDB. Stage the datafiles in Local FS and run transformations. Stage the transformed datafiles in AWS S3 storage. Load the files in Snowflake.

![img1](images/ge.png "Title")

## Lets Get started

#### Install the required packages
First step is to install the required packages
```
pip install great_expectations
pip install SQLAlchemy
pip install psycopg2-binary 
```
#### Initialize great expectations project
Next let's initialize great expectations project
```
great_expectations init
```
opt for no datasource at this point.

#### Add the data sources
Let's add the data source
##### MYSQL
Install MYSQL required packages and add the datasource:
```
pip install PyMySQL
great_expectations datasource new

What data would you like Great Expectations to connect to?
    1. Files on a filesystem (for processing with Pandas or Spark)
    2. Relational database (SQL)
: 2

Which database backend are you using?
    1. MySQL
    2. Postgres
    3. Redshift
    4. Snowflake
    5. BigQuery
    6. other - Do you have a working SQLAlchemy connection string?
: 1

Give your new Datasource a short name.
 [my_mysql_db]:

Next, we will configure database credentials and store them in the `my_mysql_db` section
of this config file: great_expectations/uncommitted/config_variables.yml:

What is the host for the MySQL connection? [localhost]:
What is the port for the MySQL connection? [3306]:
What is the username for the MySQL connection? []:
What is the password for the MySQL connection?:
What is the database name for the MySQL connection? []:
Attempting to connect to your database. This may take a moment...

Great Expectations will now add a new Datasource 'my_mysql_db' to your deployment, by adding this entry to your great_expectations.yml:

  my_mysql_db:
    credentials: ${my_mysql_db}
    data_asset_type:
      class_name: SqlAlchemyDataset
      module_name: great_expectations.dataset
    class_name: SqlAlchemyDatasource
    module_name: great_expectations.datasource

The credentials will be saved in uncommitted/config_variables.yml under the key 'my_mysql_db'

Would you like to proceed? [Y/n]:

```

##### Filesystem
Add filesystem as datasource
```
pip install fsspec
great_expectations datasource new


What data would you like Great Expectations to connect to?
    1. Files on a filesystem (for processing with Pandas or Spark)
    2. Relational database (SQL)
:1
    
What are you processing your files with?
    1. Pandas
    2. PySpark
: 1

Enter the path (relative or absolute) of the root directory where the data files are stored.
: C:\Users\xxx\PycharmProjects\great_expectations_poc\staging

Give your new Datasource a short name.
 [staging__dir]:

Great Expectations will now add a new Datasource 'staging__dir' to your deployment, by adding this entry to your great_expectations.yml:

```
##### AWS s3
Add aws s3 as datasource

flow the instructions in [docs.greatexpectations.io](https://docs.greatexpectations.io/en/latest/guides/how_to_guides/configuring_datasources/how_to_configure_a_pandas_s3_datasource.html)

```
pip install s3fs
```
1. Edit your great_expectations/great_expectations.yml file
Update your datasources: section to include a PandasDatasource.
```
datasources:
  pandas_s3:
    class_name: PandasDatasource
```

2. Load data from S3 using native S3 path-based Batch Kwargs.
Because Pandas provides native support for reading from S3 paths, this simple configuration will allow loading data sources from S3 using native S3 paths.
```
context = DataContext()
batch_kwargs = {
    "datasource": "pandas_s3",
    "path": "s3a://my_bucket/my_prefix/key.csv",
}
batch = context.get_batch(batch_kwargs, "existing_expectation_suite_name")
```
Note: This I ran in the Jupyter notebook generated by running 
```
great_expectations suite edit ge_mysql_demo_tbl.warning

paste the code in one of the cell and run just that cell.
Note: I added the datasource after creating the MYSQL expectation suite
```

##### Snowflake
Install Snowflake required packages and add the datasource
```
pip install snowflake-connector-python
pip install snowflake-sqlalchemy

great_expectations datasource new


What data would you like Great Expectations to connect to?
    1. Files on a filesystem (for processing with Pandas or Spark)
    2. Relational database (SQL)
: 2

Which database backend are you using?
    1. MySQL
    2. Postgres
    3. Redshift
    4. Snowflake
    5. BigQuery
    6. other - Do you have a working SQLAlchemy connection string?
: 4

Give your new Datasource a short name.
 [my_snowflake_db]: my_snowflake_db_new

Next, we will configure database credentials and store them in the `my_snowflake_db_new` section
of this config file: great_expectations/uncommitted/config_variables.yml:

What authentication method would you like to use?
    1. User and Password
    2. Single sign-on (SSO)
    3. Key pair authentication
: 1
What is the user login name for the snowflake connection? []:
What is the account name for the snowflake connection (include region -- ex 'ABCD.us-east-1')? []:
What is database name for the snowflake connection? (optional -- leave blank for none) []:
What is schema name for the snowflake connection? (optional -- leave blank for none) []:
What is warehouse name for the snowflake connection? (optional -- leave blank for none) []:
What is role name for the snowflake connection? (optional -- leave blank for none) []:
What is the password for the snowflake connection?:
Attempting to connect to your database. This may take a moment...

Great Expectations will now add a new Datasource 'my_snowflake_db_new' to your deployment, by adding this entry to your great_expectations.yml:

  my_snowflake_db_new:
    credentials: ${my_snowflake_db_new}
    data_asset_type:
      class_name: SqlAlchemyDataset
      module_name: great_expectations.dataset
    class_name: SqlAlchemyDatasource
    module_name: great_expectations.datasource

The credentials will be saved in uncommitted/config_variables.yml under the key 'my_snowflake_db_new'

Would you like to proceed? [Y/n]:

```
##### Great Expectations suite
let’s create our first Expectations using suite scaffold.  In order to create a new suite, we will use the scaffold command to automatically create an Expectation Suite called mytest.demo 
with the help of a built-in profiler. 
```
great_expectations suite scaffold ge_mysql_demo_tbl.warning
Heads up! This feature is Experimental. It may change. Please give us your feedback!
Select a datasource
    1. my_mysql_db
    2. my_snowflake_db
    3. pandas_s3
    4. staging__dir
: 1

Which table would you like to use? (Choose one)
    1. test1 (table)
    2. test2 (table)
    Do not see the table in the list above? Just type the SQL query
: 1

```
After selecting the table, Great Expectations will open a Jupyter notebook which will take you through the next part of the scaffold workflow.
Notebooks are a simple way of interacting with the Great Expectations Python API. Since notebooks are often less permanent, creating Expectations 
in a notebook also helps reinforce that the source of truth about Expectations is the Expectation Suite, not the code that generates the Expectations.

Let’s execute all the cells and wait for Great Expectations to open a browser window with Data Docs.
The automated Profiler connected to your data (using the Datasource you configured in the previous step), took a quick look at the contents, and produced an initial set of Expectations.

By default, Expectation Suites are stored in a JSON file in a subdirectory of your great_expectations/ expectations folder. 
To edit an expectation suite and open data docs
```
great_expectations suite edit ge_mysql_demo_tbl.warning
```
![img2](images/suite-1.png "Title")

![img3](images/suite-2.png "Title")

#### Data Validation
##### Create a new checkpoint
```
great_expectations checkpoint new my_checkpoint ge_mysql_demo_tbl.warning
Heads up! This feature is Experimental. It may change. Please give us your feedback!
Select a datasource
    1. my_mysql_db
    2. my_snowflake_db
    3. pandas_s3
    4. staging__dir
: 4

Would you like to:
    1. choose from a list of data assets in this datasource
    2. enter the path of a data file
: 2

Enter the path of a data file (relative or absolute, s3a:// and gs:// paths are ok too)
: C:\Users\xxxx\PycharmProjects\great_expectations_poc\staging\ge_mysql_demo_tbl_202101061644.csv
A checkpoint named `my_checkpoint` was added to your project!
  - To edit this checkpoint edit the checkpoint file: C:\Users\18329\PycharmProjects\great_expectations_poc\great_expectations\checkpoints\my_checkpoint.yml
  - To run this checkpoint run `great_expectations checkpoint run my_checkpoint`


```
I created checkpoints for all the data sources.
##### Run a checkpoint in the cli
````
great_expectations checkpoint run my_checkpoint
````

##### Hosting Data Docs
Data Docs is an HTML documentation created automatically by great expectations. It displays expectation suites and validation results in a human-readable form.

Use the following CLI command to built the Data Docs site and provide the path to the index page.
```
great_expectations docs build --site-name local_site

The following Data Docs sites will be built:

 - local_site: file://C:\Users\xxxx\PycharmProjects\great_expectations_poc\great_expectations\uncommitted/data_docs/local_site/index.html

Would you like to proceed? [Y/n]: Y

Building Data Docs...

Done building Data Docs

```
![img4](images/datadoc.png "Title")

#### Great Expectations in Prefect Workflow
Integrate Great Expectations in Prefect Workflow

##### Add Prefect task to run checkpoints
```
pip install prefect

add tasks as shown in example in great_expectations\great_expectations_flow.py and great_expectations_simple.py
```
##### Run Prefect Workflow
We will use server orchestration for our use case. The local server requires Docker and Docker Compose to be installed. If you have Docker Desktop on your machine, you've got both of these.
```
Step 1: Before running the server for the first time, run:
prefect backend server

Step 2: Next, to start the server, UI, and all required infrastructure, run:
prefect server start

Step 3: The executing flows from the server requires at least one Prefect Agent to be running: 
prefect agent local start

Step 4: Register a flow:
cd great_expectations
python.exe C:/Users/xxxx/PycharmProjects/great_expectations_poc/great_expectations/great_expectations_flow.py

view the workflow in browser localhost:8080

Step 5: Register a flow:

Step 6: Execute a Flow Run
We will use the UI to execute a run. On the flow page click page click "Quick Run" in the upper-right corner.

```
![img5](images/prefect.png "Title")

#### Miscellaneous

* The data source configurations are stored in uncommitted\great_expectations.yml.
* The data source secrets are stored in config_variables.yml. The default location of the file is uncommitted\config_variables.yml. The file can be stored in any location, just update the "config_variables_file_path" key value in uncommitted\great_expectations.yml .
* The expectation suite JSON is in expectations\ .
* The checkpoint YAMLs are in the checkpoints\ folder .
* The checkpoints and validations can be configured to store in any on-prem or cloud store.
* The Suites generated by the scaffold command is not meant to be production suites.
* By default, Data Docs are stored locally, in an uncommitted directory. This is great for individual work, but not good for collaboration. A better pattern is usually to deploy to a cloud-based blob store (S3, GCS, or Azure blob store), configured to share a static website.
* Great Expectations can be deployed with workflow orchestration services like Airflow, Google Cloud Composer, Astronomer.



