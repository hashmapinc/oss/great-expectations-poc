import csv
import datetime

from prefect import Flow, Parameter, task, Client
# from prefect.schedules import IntervalSchedule
from prefect.tasks.great_expectations import RunGreatExpectationsValidation
from prefect.artifacts import create_link, create_markdown


@task()
def mysql_extract():
    print("Data Extracted from MySQL")


@task
def fs_stage():
    print("Data Staged in Local FS")


@task
def cloud_stage():
    print("Data Staged in Cloud Storage")


@task
def snowflake_load():
    print("Data Loaded to Snowflake")


def register_flow(flow):
    client = Client()
    client.create_project(project_name="great_expectation_poc_1")
    flow.register(project_name="great_expectation_poc_1")


def build_flow():
    """
    schedule = IntervalSchedule(
        start_date=datetime.datetime.now() + datetime.timedelta(seconds=1),
        interval=datetime.timedelta(seconds=5)
    )
    """
    # with Flow("great expectations example flow", schedule=schedule) as flow:
    with Flow("great expectations example flow-new") as flow:
        # MYSql data extract
        create_markdown("MYSql data extract")
        my_sql_data = mysql_extract()

        # Local FS staging
        create_markdown("Local FS staging")
        fs_file = fs_stage(upstream_tasks=[my_sql_data])

        # Local FS file data validation
        create_markdown("Local FS file data validation")
        stg_chk = Parameter("stg_chk" , default=["staging.chk"])
        ge_task = RunGreatExpectationsValidation(run_name="fs_stage_validation")
        validations_1 = ge_task.map(stg_chk)
        validations_1.set_upstream(fs_file)
        create_link("http://prefect.io/")

        # Cloud storage staging
        create_markdown("Cloud storage staging")
        cloud_file = cloud_stage(upstream_tasks=[validations_1])

        # Cloud storage file data validation
        create_markdown("Cloud storage file data validation")
        cloud_chk = Parameter("cloud_chk", default=["cloudstaging.chk"])
        ge_task2 = RunGreatExpectationsValidation(run_name="cloud_stage_validation")
        validations_2 = ge_task2.map(cloud_chk)
        validations_2.set_upstream(cloud_file)
        create_link("http://prefect.io/")

        # Snowflake data load
        create_markdown("Snowflake data load")
        snowflake_data = snowflake_load(upstream_tasks=[validations_2])

        # Snowflake data validation
        create_markdown("Snowflake data validation")
        dest_chk = Parameter("dest_chk", default=["destination.chk"])
        ge_task3 = RunGreatExpectationsValidation(run_name="snowflake_destination_validation")
        validations_3 = ge_task3.map(dest_chk)
        validations_3.set_upstream(snowflake_data)
        create_link("http://prefect.io/")

        return flow


if __name__ == "__main__":
    flow = build_flow()
    """
    flow.run(
        staging_checkpoint=["staging.chk"],
        cloudstaging_checkpoint=["cloudstaging.chk"],
        destination_checkpoint=["destination.chk"]
    )
    """
    flow.run()

    register_flow(flow)
